package app.boyorgirl.predictorquizz.free;

/**
 * Created by FZSM on 2019/03/03.
 */

//Represents an answer
public class Answer {
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getQuestionNumber() {
        return questionNumber;
    }

    public void setQuestionNumber(int questionNumber) {
        this.questionNumber = questionNumber;
    }

    public String getResultValue() {
        return resultValue;
    }

    public void setResultValue(String resultValue) throws Exception {
        this.resultValue = resultValue;
        this.ponderateValue = convertValueToNumber(resultValue);
    }

    //this function converts current Value (string) to a ponderated number
    //in the range [-9,9]
    //20190323: when more the left more the probability of being GIRL,
    //When more the right more the chance of being BOY
    //When more the middle more the chance of NON-CONCLUSIVE or useless results.
    //9=100% chance of being girl
    //i=100% chance of being boy
    //0= non conclusive
    //5 = the answer has 100/5% of being true for girl, 0% for boy. This means this question has no much chance of being a true legend
    //d= the answer has 100/4% of being true for boy , 0% for girl. This means this question has no much chance of being a true legend.
    private int convertValueToNumber (String value) throws Exception {
        String table ="9876543210abcdefghi";
        int pos = table.indexOf(value.toLowerCase());
        //if not found, we have an error
        if (pos==-1) {
            throw new Exception("convertValueToNumber error: character " + value + " not found");
        }
        return pos-9;
    }

    public int getPonderateValue() {
        return ponderateValue;
    }

    /*
    public void setPonderateValue(int ponderateValue) {
        this.ponderateValue = ponderateValue;
    }
    */

    int questionNumber;
    String text;
    String resultValue; //value range from 9....0a...i
    int    ponderateValue; //value range [-9...9]
}
