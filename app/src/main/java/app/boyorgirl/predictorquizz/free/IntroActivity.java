package app.boyorgirl.predictorquizz.free;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;

import app.boyorgirl.predictorquizz.free.Intro.Slide01Fragment;
import app.boyorgirl.predictorquizz.free.Intro.Slide02Fragment;
import app.boyorgirl.predictorquizz.free.Intro.Slide05Fragment;
import app.boyorgirl.predictorquizz.free.Intro.Slide06Fragment;

//import boyorgirlpregnancyquizz.thekingmobile.com.boyorgirlpregnancyquizz.R;

/**
 * Created by i7 on 2019/03/08.
 */

public class IntroActivity extends AppIntro {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Add your slide's fragments here
        // AppIntro will automatically generate the dots indicator and buttons.

        Fragment slp1 = new Fragment();

        addSlide(new Slide01Fragment());
        addSlide(new Slide02Fragment());
        //addSlide(new Slide03Fragment());
        //addSlide(new Slide04Fragment());
        addSlide(new Slide05Fragment());
        addSlide(new Slide06Fragment());
        //addSlide(new Slide07Fragment());

        /*
        SliderPage sliderPage3 = new SliderPage();
        sliderPage3.setTitle("Simple, yet Customizable");
        sliderPage3.setDescription("The library offers a lot of customization, while keeping it simple for those that like simple.");
        sliderPage3.setImageDrawable(R.drawable.cat02);
        sliderPage3.setBgColor(Color.TRANSPARENT);
        addSlide(AppIntroFragment.newInstance(sliderPage3));
        // Instead of fragments, you can also use our default slide
        // Just set a title, description, background and image. AppIntro will do the rest
        addSlide(AppIntroFragment.newInstance("My title", "My description", R.drawable.cat01, Color.parseColor("#001122")));
*/
        // OPTIONAL METHODS

        // Override bar/separator color
        setBarColor(Color.parseColor("#3F51B5"));
        setSeparatorColor(Color.parseColor("#2196F3"));

        // SHOW or HIDE the statusbar
        showStatusBar(true);

        // Edit the color of the nav bar on Lollipop+ devices
//        setNavBarColor(Color.parseColor("#3F51B5"));
        setNavBarColor(R.color.colorAccent);

        // Hide Skip/Done button
       // showSkipButton(false);
        //showDoneButton(false);

        // Turn vibration on and set intensity
        // NOTE: you will probably need to ask VIBRATE permisssion in Manifest
        setVibrate(true);
        setVibrateIntensity(30);

        // Animations -- use only one of the below. Using both could cause errors.
        setFadeAnimation(); // OR
        showSkipButton(false);  //Avoid skip button, since this is one time only
//Done button also?
        //showDoneButton(false);


        // Permissions -- takes a permission and slide number
       // askForPermissions(new String[]{Manifest.permission.CAMERA}, 3);
    }

    @Override
    public void onSkipPressed() {
        // Do something when users tap on Skip button.
    }

    @Override
    public void onNextPressed() {
        // Do something when users tap on Next button.
    }

    @Override
    public void onDonePressed() {
        // Do something when users tap on Done button.
        finish();
    }


    //Super trick: If ound that ALL slides are running at once. It means
    //I don't have a sure way of firing animations when a fragment is slided.
    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
         //newFragment.onViewCreated()
        //If newFragment is null it means we reache the final slide, so we don't need to activate any fragment.
        if (newFragment==null) return;
        newFragment.onViewCreated(null ,new Bundle());
    }

}