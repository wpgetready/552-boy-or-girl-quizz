package app.boyorgirl.predictorquizz.free.Intro;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import app.boyorgirl.predictorquizz.free.R;
import app.boyorgirl.predictorquizz.free.Tools.Tools;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;



/**
 * A simple {@link Fragment} subclass.
 */
public class Slide01Fragment extends Fragment {

    TextView txt01,txt02;
 ImageView logo;

    public Slide01Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        return inflater.inflate(R.layout.fragment_slide01, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        //TODO: wait one,two seconds and later display de heart.
        //Also please solve the size issues, this is bugging me around...
        /*
        LottieAnimationView animationView =(LottieAnimationView) getView().findViewById(R.id.LottieAnim);
        animationView.setAnimation(R.raw.twitterheart);
        animationView.loop(false);
        animationView.playAnimation();
        */
        //logo = getView().findViewById(R.id.imgLogo);
        logo = getView().findViewById(R.id.imgLogo);
        //Bug: unable to load the image directly,
        logo.setImageBitmap(Tools.decodeSampledBitmapFromResource(getResources(),R.drawable.logosquare,256,256));
        txt01 = getView().findViewById(R.id.txt01);
        txt02 = getView().findViewById(R.id.txt02);

        //set alpha for everyone
        logo.setAlpha(0.0f);
        txt01.setAlpha(0.0f);
        txt02.setAlpha(0.0f);

        //define appearance
        ObjectAnimator a1 = ObjectAnimator.ofFloat(txt01, "alpha", 1f);
        a1.setDuration(1500);


        ObjectAnimator a2 = ObjectAnimator.ofFloat(logo, "alpha", 1f);
        a2.setDuration(1500);

        ObjectAnimator a3 = ObjectAnimator.ofFloat(txt02, "alpha", 1f);
        a3.setDuration(1500);

        AnimatorSet as = new AnimatorSet();

        as.playSequentially(a1,a2,a3);
        as.start();

    }


}
