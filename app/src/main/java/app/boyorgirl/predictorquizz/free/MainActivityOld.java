package app.boyorgirl.predictorquizz.free;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.Button;
import android.widget.TextView;

//import boyorgirlpregnancyquizz.thekingmobile.com.boyorgirlpregnancyquizz.R;

public class MainActivityOld extends AppCompatActivity {

    Button btnStart;
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



//        Quizz q =new Quizz(this);
//        q.checkQuestionIntegrity(65,1);

        if (false) {
            Intent intent = new Intent(this,PieChartQuizz.class);
            intent.putExtra("perCent",76f);
            intent.putExtra("isBoy",false);
            intent.putExtra("answers","empty for the moment");
            startActivity(intent);
            return;
        }

        title = findViewById(R.id.txtTitle);
        btnStart = findViewById(R.id.btnStart);

        Typeface tf = Typeface.createFromAsset(getAssets(),"OpenSans-Regular.ttf");
        title.setTypeface(tf,Typeface.BOLD);
        //After reading
        //https://developer.android.com/guide/topics/graphics/prop-animation#object-animator
        //AnimatorSet es exactly the answer, combining several animations one before another.


        //Fade in
        ObjectAnimator a1 = ObjectAnimator.ofFloat(title, "alpha", 1f);
        a1.setDuration(1500);

        //Do nothing for a second
        ObjectAnimator a2 = ObjectAnimator.ofFloat(title, "alpha", 1f);
        a2.setDuration(3000);

        //Fade out
        ObjectAnimator a3 = ObjectAnimator.ofFloat(title, "alpha", 1f,0f);
        a3.setDuration(1000);

        //animation2.start();
        final Context ctx =this; //Need to declare here, because context will be used inside a listener (there is a visibility problem...)
        AnimatorSet as = new AnimatorSet();
        //as.playSequentially(a1,a2,a3);
        as.playSequentially(a1,a2);
        as.start();
        as.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationEnd(Animator animation) {
                //Important: animation ended is fired when ALL animations played are finished.
                //Make an only-once intro for this app
                SharedPreferences sp = getSharedPreferences("prefs", Context.MODE_PRIVATE);
                if (!sp.getBoolean("Intro",false)){
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putBoolean("Intro",true);  //WArning; this is for check de Intro. FIX IT LATER!!!!!!!!!!!
                    editor.apply();
                    Intent intent = new Intent(ctx,IntroActivity.class);
                    startActivity(intent);
                }
                ViewPropertyAnimator vpa = btnStart.animate().alpha(1.0f).setDuration(1000);
                vpa.setListener(new Animator.AnimatorListener() {

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        btnStart.setEnabled(true);
                    }

                    @Override
                    public void onAnimationStart(Animator animation) { }
                    @Override
                    public void onAnimationCancel(Animator animation) { }

                    @Override
                    public void onAnimationRepeat(Animator animation) { }
                });

            }

            @Override
            public void onAnimationStart(Animator animation) {   }

            @Override
            public void onAnimationCancel(Animator animation) { }

            @Override
            public void onAnimationRepeat(Animator animation) { }
        });



//       Intent intent = new Intent(this,IntroActivity.class);
//
//     startActivity(intent);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx,QuizzActivity.class);
                startActivity(intent);
            }
        });
    }

    /*
    public void startQuizz (View view) {
       Intent intent = new Intent(this,QuizzActivity.class);
       startActivity(intent);
    }
    */
}
