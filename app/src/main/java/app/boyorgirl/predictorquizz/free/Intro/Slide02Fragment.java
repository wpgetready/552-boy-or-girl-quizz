package app.boyorgirl.predictorquizz.free.Intro;


import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.github.paolorotolo.appintro.ISlideBackgroundColorHolder;

import app.boyorgirl.predictorquizz.free.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Slide02Fragment extends Fragment  implements ISlideBackgroundColorHolder {

TextView txt03;
TextView txt04;

    public Slide02Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_slide02, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        //if somebody is calling this with a null view, then it's me, proceed.
        if (view !=null)return;

        //I am calling the fragment specila initialization. Proceed with it.

        txt03 = getView().findViewById(R.id.txt03);
        txt04 = getView().findViewById(R.id.txt04);

        final LottieAnimationView animationView =(LottieAnimationView) getView().findViewById(R.id.LottieAnim);
         animationView.setAnimation(R.raw.twitterheart);

        //set object to invisible
        txt03.setAlpha(0.0f);
        txt04.setAlpha(0.0f);
        animationView.setAlpha(0.0f);

        ObjectAnimator a1 = ObjectAnimator.ofFloat(txt03, "alpha", 1f);
        a1.setDuration(1500);

        ObjectAnimator a2 = ObjectAnimator.ofFloat(txt04, "alpha", 1f);
        a2.setDuration(1500);

        AnimatorSet as = new AnimatorSet();

        as.playSequentially(a1,a2);
        as.start();

        as.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {}

            @Override
            public void onAnimationEnd(Animator animation) {
                animationView.setAlpha(1.0f);
                animationView.loop(false);
                animationView.playAnimation();
            }

            @Override
            public void onAnimationCancel(Animator animation) {}

            @Override
            public void onAnimationRepeat(Animator animation) {}
        });
    }

    @Override
    public int getDefaultBackgroundColor() {
        // Return the default background color of the slide.
        return Color.parseColor("#000000");
    }

    @Override
    public void setBackgroundColor(int backgroundColor) {
        // Set the background color of the view within your slide to which the transition should be applied.
        View layoutContainer =getView();
        if (layoutContainer != null) {
            layoutContainer.setBackgroundColor(backgroundColor);
        }
    }
}
