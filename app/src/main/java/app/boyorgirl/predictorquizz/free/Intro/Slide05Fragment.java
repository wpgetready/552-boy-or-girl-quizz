package app.boyorgirl.predictorquizz.free.Intro;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.paolorotolo.appintro.ISlideBackgroundColorHolder;

import app.boyorgirl.predictorquizz.free.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Slide05Fragment extends Fragment implements ISlideBackgroundColorHolder {


    TextView txt05,txt06,txt07;

    public Slide05Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        return inflater.inflate(R.layout.fragment_slide05, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (view !=null) return; //a way of detecting manual update

        txt05= getView().findViewById(R.id.txt05);
        txt06= getView().findViewById(R.id.txt06);
        txt07= getView().findViewById(R.id.txt07);

        txt05.setAlpha(0.0f);
        txt06.setAlpha(0.0f);
        txt07.setAlpha(0.0f);

        ObjectAnimator a1 = ObjectAnimator.ofFloat(txt05, "alpha", 1f);
        a1.setDuration(1500);

        ObjectAnimator a2 = ObjectAnimator.ofFloat(txt06, "alpha", 1f);
        a2.setDuration(1500);

        ObjectAnimator a3 = ObjectAnimator.ofFloat(txt07, "alpha", 1f);
        a3.setDuration(1500);


        AnimatorSet as = new AnimatorSet();

        as.playSequentially(a1,a2,a3);
        as.start();




    }

    @Override
    public int getDefaultBackgroundColor() {
        // Return the default background color of the slide.
        return Color.parseColor("#000000");
    }

    @Override
    public void setBackgroundColor(int backgroundColor) {
        // Set the background color of the view within your slide to which the transition should be applied.
        View layoutContainer =getView();
        if (layoutContainer != null) {
            layoutContainer.setBackgroundColor(backgroundColor);
        }
    }
}
