package app.boyorgirl.predictorquizz.free.Tools;

/*
20190322: The following code is from
https://android-arsenal.com/details/1/5649  or https://github.com/karanvs/hiddenshot
The problem with this library is that it didn't work with API 24+, and also there is no direct way from that API version
to access a file without asking permissions.

In short, there a lot things changed after this code was written:
(and this is from 2016 according to https://en.wikipedia.org/wiki/Android_version_history)

The solution is broken in several parts:
1-Adding permissions on AndroidManifest as already explained in the library:
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
 <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />

2- HOWEVER, this is NOT enough anymore.
https://developer.android.com/training/permissions/requesting
This is because we are requesting a dangerous permission. The solution is asking user permissions to do so:
https://stackoverflow.com/questions/8854359/exception-open-failed-eacces-permission-denied-on-android
This is implemented on DemoBase.java where PieChartQuizz.java inherits from (otherwise should be directly from the activity)


3-Modify the AndroidManifest
See the final explanation here
https://inthecheesefactory.com/blog/how-to-share-access-to-file-with-fileprovider-on-android-nougat/en
which includes:
 -Adding a provider_paths.xml under res/xml folder (also explained in the link above)

4-Finally, modify how the class access to the file using FileProvider.getUriForFile
That was a MANDATORY change inside this class , see Saveshot method

After ALL these changes, we are enabled to send a shot of the screen.
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.core.content.FileProvider;
import app.boyorgirl.predictorquizz.free.BuildConfig;

import static android.content.ContentValues.TAG;

/**
 * The type HiddenShot class.
 */
public class HiddenShot {
    private static final HiddenShot ourInstance = new HiddenShot();

    public static HiddenShot getInstance() {
        return ourInstance;
    }

    private boolean isActive = false;

    private Handler mhandler;

    private TakeShot takeShot;

    private HiddenShot() {
    }

    public Bitmap buildShot(Activity activity) {
        View v = activity.getWindow().getDecorView().getRootView();
        v.setDrawingCacheEnabled(true);
        v.buildDrawingCache(true);
        Bitmap b = Bitmap.createBitmap(v.getDrawingCache());
        v.setDrawingCacheEnabled(false); // clear drawing cache
        return b;
    }

    public Uri saveShot(Context context, Bitmap image, String filename) {
        File bitmapFile = getOutputMediaFile(filename);
        if (bitmapFile == null) {
            Log.d(TAG, "Error creating media file, check storage permissions: ");
            return null;
        }
        try {
            FileOutputStream fos = new FileOutputStream(bitmapFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
            MediaScannerConnection.scanFile(context, new String[] { bitmapFile.getPath() },
                    new String[] { "image/jpeg" }, null);
            //20190322:FZSM  changed return Uri.fromFile(bitmapFile); see documentation above.
            //Notice the authority (second parameter) is EXACTLY the same as defined in AndroidManifest.xml
            return FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID+".provider",bitmapFile);
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
            return null;
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
            return null;
        }
    }

    private File getOutputMediaFile(String filename) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDirectory = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                        + File.separator);
        // Create the storage directory if it does not exist
        if (!mediaStorageDirectory.exists()) {
            if (!mediaStorageDirectory.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
        File mediaFile;
        String mImageName = filename + timeStamp + ".jpg";
        mediaFile = new File(mediaStorageDirectory.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public void buildContinousShot(Activity activity, long millisecond) {
        isActive=true;
        mhandler = new Handler();
        takeShot=new TakeShot(activity,millisecond);
        mhandler.postDelayed(takeShot, millisecond);
    }

    class TakeShot implements Runnable {
        private final Activity activity;
        private long millisec;

        TakeShot(Activity activity,long millisec) {
            this.activity = activity;
            this.millisec=millisec;
        }

        @Override public void run() {
            if (isActive) {
                Bitmap bitmap = buildShot(activity);
                saveShot(activity, bitmap, "shot");
                mhandler.postDelayed(takeShot, millisec);
            }
        }
    }


    public void stopContinousShot()
    {
        if(isActive)
        {
            isActive=false;
            mhandler.removeCallbacks(takeShot);
        }
    }

    public void buildShotAndShare(Activity activity) {
        Bitmap bitmap = buildShot(activity);
        Uri uri = null;
        try {
            uri = saveShot(activity, bitmap, "screenshot");
        } catch (NullPointerException e) {
            Log.e("error", "null uri for file");
            return;
        }
        shareShot("Choose an app", activity, uri);
    }

    private void shareShot(String s, Activity activity, Uri uri) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.setType("image/*");
        try {
            activity.startActivity(Intent.createChooser(shareIntent, "Choose an app"));
        } catch (android.content.ActivityNotFoundException ex) {

            Toast.makeText(activity, "There is no app that can handle sharing", Toast.LENGTH_LONG).show();
        }
    }

    public void buildShotAndShare(Activity activity,String shareMsg) {
        Bitmap bitmap = buildShot(activity);
        Uri uri = null;
        try {
            uri = saveShot(activity, bitmap, "screenshot");
        } catch (NullPointerException e) {
            Log.e("error", "null uri for file");
            return;
        }
        shareShot("Choose an app", activity, uri,shareMsg);
    }

    private void shareShot(String s, Activity activity, Uri uri,String message) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.putExtra(Intent.EXTRA_TEXT,message);
        shareIntent.setType("image/*");
        try {
            activity.startActivity(Intent.createChooser(shareIntent, "Choose an app"));
        } catch (android.content.ActivityNotFoundException ex) {

            Toast.makeText(activity, "There is no app that can handle sharing", Toast.LENGTH_LONG).show();
        }
    }
}
