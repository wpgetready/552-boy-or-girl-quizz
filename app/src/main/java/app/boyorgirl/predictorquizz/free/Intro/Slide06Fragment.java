package app.boyorgirl.predictorquizz.free.Intro;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.paolorotolo.appintro.ISlideBackgroundColorHolder;

import app.boyorgirl.predictorquizz.free.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Slide06Fragment extends Fragment implements ISlideBackgroundColorHolder {

    TextView txt08,txt09,txt10;
    ImageView imgLogo;

    public Slide06Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        return inflater.inflate(R.layout.fragment_slide06, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (view!=null) return;

        txt08 = getView().findViewById(R.id.txt08);
        txt09 = getView().findViewById(R.id.txt09);
        txt10 = getView().findViewById(R.id.txt10);
        imgLogo = getView().findViewById(R.id.imgLogoEnd);

        txt08.setAlpha(0.0f);
        txt09.setAlpha(0.0f);
        txt10.setAlpha(0.0f);
        imgLogo.setAlpha(0.0f);

        ObjectAnimator a1 = ObjectAnimator.ofFloat(txt08, "alpha", 1f);
        a1.setDuration(1500);

        ObjectAnimator a2 = ObjectAnimator.ofFloat(txt09, "alpha", 1f);
        a2.setDuration(2500);

        ObjectAnimator a3 = ObjectAnimator.ofFloat(txt10, "alpha", 1f);
        a3.setDuration(2500);

        ObjectAnimator a4 = ObjectAnimator.ofFloat(imgLogo, "alpha", 1f);
        a4.setDuration(1000);

        AnimatorSet as = new AnimatorSet();

        as.playSequentially(a1,a2,a3,a4);
        as.start();

    }

    @Override
    public int getDefaultBackgroundColor() {
        // Return the default background color of the slide.
        return Color.parseColor("#000000");
    }

    @Override
    public void setBackgroundColor(int backgroundColor) {
        // Set the background color of the view within your slide to which the transition should be applied.
        View layoutContainer =getView();
        if (layoutContainer != null) {
            layoutContainer.setBackgroundColor(backgroundColor);
        }
    }
}
