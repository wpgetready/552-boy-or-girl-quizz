package app.boyorgirl.predictorquizz.free;

import android.content.Context;
import androidx.annotation.NonNull;
import android.util.Log;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//import boyorgirlpregnancyquizz.thekingmobile.com.boyorgirlpregnancyquizz.R;

/**
 * Created by i7 on 2019/03/03.
 * References
 * https://stackoverflow.com/questions/31922197/number-of-strings-in-strings-xml
 */

public class Quizz {
    enum validQuestion {
        QUESTION_OK,
        QUESTION_TEXT_EMPTY,
        QUESTION_RESULTS_EMPTY,
        QUESTION_RESULTS_ANSWER_MISMATCH,
        QUESTION_EXCEPTION
    }

    private int MAX_QUESTIONS =14;
    private int MAX_CATEGORIES=20;

    Context mContext;

    public Quizz(Context c) {
        if (c==null) {
            Log.e("ERROR","El contexto está vacío, NO SIRVE!!!");
        }
        this.mContext = c;
    }

    //Reset the quizz and build a new one
    public List<Question> buildQuizz(){
        List<Question> questions = new ArrayList<Question>();

        //Step 1: get how many categories do we have
        //Curerntly I decided making 10 questions, since there are too many (20)
        List<Integer> randomCategories = randomShuffle();
        Question q;


        //Loop every category
        //In the first instance, we are going to select just 10
        for (Integer item: randomCategories) {
            try {
                q= getRandomQuestionForCategory(item);
                questions.add(q);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    return questions;
    }

    //Get a random list of 10 values, that will be used to select the posible categories.
    public List<Integer> randomShuffle() {
        List<Integer> solution = new ArrayList<>();
        int catNumber = getCategoriesNumber();
        for (int i = 1; i <= catNumber; i++) {
            solution.add(i);
        }
        Collections.shuffle(solution);
        //If 0 to 10 = 10 results
        //if 1 to 11 = 10 results
        return solution.subList(1,MAX_QUESTIONS+1);
    }

    //At first it won't be dinamic but it should be ok.
    private int getCategoriesNumber(){
        return MAX_CATEGORIES;
    }

    //There are n categories. Every category is ONE question, but it is rephrased in many ways, having its own answers.
    //In short, when we select a random question in one category, we are selecting the SAME question but it was rewritten differently.
    //This concept is for having a questionnare that it is ALWAYS different, but basically the same questions
    private Question getRandomQuestionForCategory(int category) throws IllegalAccessException {
        //1-get how many questions are for this category
        //Podria asumir un numero inicialmente, de 1 en adelante.
        //Entonces , iteramos por la cantidad de categorias.
        Question q;
        int answersNumber = getCategoryAmount(category);
        if (answersNumber==1) {
             q =  buildQuestion(category,1); //there is only one, select that one
        } else
        {
            //Make a random number between this value, considering questions go from 1 to n (NOT zero)
            int randomQuestion = (int)(Math.random() * answersNumber + 1);
            q =  buildQuestion(category,randomQuestion);
        }
        return q;
    }

    //return how many questions are for a given category.
    public int getCategoryAmount(int category) {

        String cat = catFormat(category); //get only de cat number
        Field[] fields = R.string.class.getFields();
        String str_key,str_value;
        int counter=0;
        for (Field field:fields) {
            str_key = field.getName();
            if (!str_key.startsWith(cat)) continue;
            if (str_key.length()!=6) continue; //This limit this procedure to 99 questions
            counter++;
        }
        return counter;
    }

    //format the category: Cxx where xx is the category number
    private String catFormat(int category) {
        return "C" + String.format("%02d", category);
    }

    //format question Qyy where yy is he question number
    private String questionFormat(int question) {
        return "Q" + String.format("%02d",question);
    }

    //Returns how many Answers are there for a given category and question
    //This method uses Reflection for finding proper answers for a question
    public int getAnswerQuantity (int category, int question) {
        String cat = catFormat(category)  + questionFormat(question); //format the category/question
        Field[] fields = R.string.class.getFields();
        String str_key;
        int counter=0;
        for (Field field:fields) {
            str_key = field.getName();
            if (!str_key.startsWith(cat)) continue; //Ignore any answer from other category/question
            if (str_key.endsWith("Results")) continue; //Ignore Results from this question
            if (str_key.length()==6) continue; //Ignore the question itself (6 chars)
            counter++;
        }
        return counter;
    }

    //A question is correct if...
    //It has a question CxxQyy
    //Results length matches with Answers length
    public String getQuestion(int category, int number) throws IllegalAccessException {
        //Get the question format
        String cat = catFormat(category)  + questionFormat(number); //format the category/question
        String result =getStringValueReflection(cat);
        return result;
    }

    //Get the answers for a specific question.
    //The answers is a single string, where every char correspond an answer value.
    public String getAnswerResults(int category,int number) throws IllegalAccessException {
        String cat = catFormat(category)  + questionFormat(number) + "Results"; //format the category/question
        String result =getStringValueReflection(cat);
        return result;
    }

    //Get the string value for a string resource using Reflection
    //Returns ? if not found
    @NonNull
    private String getStringValueReflection(String resource) throws IllegalAccessException {
        Field[] fields = R.string.class.getFields();
        String str_key,str_value;
        int counter=0;
        for (Field field:fields) {
            if (resource.equals(field.getName())) {
                int id = field.getInt(null);
                return mContext.getString(id);
            }
        }
        return "?";
    }

    //Build the question object from the proper data
    public Question buildQuestion( int category, int number) throws IllegalAccessException {
        Question q = new Question();
        q.setCategory(category);
        q.setNumber(number);
        q.setText(getQuestion(category,number)); //get the text for the question
        //Current version just one image for category (well this is 20 images...)
        q.setImgResource(getImgResource(category));
        buildAnswer(q);
        return q;
    }

    //Currently, get the images resources in hard mode way.
    private int getImgResource (int category) {
       int[] imgCategory ={R.drawable.cat01,R.drawable.cat02,R.drawable.cat03,R.drawable.cat04,
                           R.drawable.cat05,R.drawable.cat06,R.drawable.cat07,R.drawable.cat08,
                           R.drawable.cat09,R.drawable.cat10,R.drawable.cat11,R.drawable.cat12,
                           R.drawable.cat13,R.drawable.cat14,R.drawable.cat15,R.drawable.cat16,
                           R.drawable.cat17,R.drawable.cat18,R.drawable.cat19,R.drawable.cat20
       };
       return imgCategory[category-1];
    }

    //Build the answer collection for a specific question
    //This method has too many points of failure. Find a way to fix it.
    private void buildAnswer ( Question q) throws IllegalAccessException {
        String question = catFormat(q.getCategory()) + questionFormat(q.getNumber());
        Field[] fields = R.string.class.getFields();
        //Get the Results, a specific string which answers values for every question
        String results = getAnswerResults(q.getCategory(), q.getNumber());
        String str_key, str_value;
        int counter = 0;
        for (Field field : fields) {
            str_key = field.getName();
            if (!str_key.startsWith(question))
                continue;//Ignore any answer from other category/question
            if (str_key.endsWith("Results")) continue;  //Ignore Results from this question
            if (str_key.length() == 6) continue;          //Ignore the question itself(6 chars)
            int id = field.getInt(null);
            Answer a = new Answer();
            a.setText(mContext.getString(id));
            //Get the answer number
            int number = Integer.parseInt(field.getName().replace(question +"A", ""));
            a.setQuestionNumber(number);
            //Get the answer value from the results.
            try {
                a.setResultValue(String.valueOf(results.charAt(number-1)));
            } catch (Exception e) {
                //This error is related with wrong values in strings.xml, specifically
                //Results values are outside range 9...0a....i
                Log.e("ERROR", "Error when assigning values:" + e.getMessage());
            }
            q.addAnswer(a);
        }
    }

    //Checks if the question is Valid.
    public validQuestion isQuestionValid(int category,int question){
        Quizz quizz =new Quizz(this.mContext);
        try {
            String q = quizz.getQuestion(category,question);
            if (q.equals("?")) {
                return validQuestion.QUESTION_TEXT_EMPTY;
            }
            q = quizz.getAnswerResults(category, question);
            if (q.equals("?")) {
                return validQuestion.QUESTION_RESULTS_EMPTY;
            }
            int   aQuantity = quizz.getAnswerQuantity(category, question);
            if (q.length()!=aQuantity) {
                return validQuestion.QUESTION_RESULTS_ANSWER_MISMATCH;
            }
            return validQuestion.QUESTION_OK;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            Log.e("ERROR", "Error when validQuestion: " + e.getMessage());
            return validQuestion.QUESTION_EXCEPTION;
        }
    }
    }

