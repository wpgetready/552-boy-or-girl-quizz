package app.boyorgirl.predictorquizz.free;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

//20190605: Ooops, DialogFragment comes with androidfx.fragment.app. First iteration I will remove this reference
//import android.support.v4.app.DialogFragment;


/**
 *
 * 20190605:
 * Reference 1: https://guides.codepath.com/android/using-dialogfragment
 * Tip: use New->Fragment->Fragment(Blank) to build proper Java class and related Layout, which turned out to be fragment_dialog.xml
 * 20190606: Trying to implement an interface , this way the app wait for closing the fragment to continue (otherwise, the fragment open and the app continues in background.
 * There is no harm done, however it is not good looking , but weird.)
 */
public class DialogFrag extends DialogFragment {

    private TextView dialogLabel;
    private TextView dialogText;
    private Button btnOk;
    private TextView txtSite;

    OnDialogDismissListener mCallback;
    public interface OnDialogDismissListener{
        public void onDialogDismissListener(int position);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnDialogDismissListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnDialogDismissListener");
        }
    }



    public DialogFrag() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //https://stackoverflow.com/questions/12322356/how-to-make-dialogfragment-modal
        //Well, this is not what I intended: I want to FREEZE execution before continuing the next question
        //setStyle(STYLE_NO_FRAME,0); This is set the window to transparent. OK this is something like I was looking for: putting an image behind we can 'mimick' frames borderless
        //and with any form....
        //THIS is what I was looking for:
        // https://stackoverflow.com/questions/16883578/activity-to-wait-for-dialog-fragment-input
        //TEST IT!!!!!!!

    }

    public static DialogFrag newInstance(String label,String text) {
        DialogFrag frag = new DialogFrag();
        Bundle args = new Bundle();
        args.putString("label", label);
        args.putString("text", text);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dialog, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnOk = view.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onDialogDismissListener(0);
                dismiss();
            }
        });
        // Get field from view
        dialogLabel =view.findViewById(R.id.dialog_label);
        dialogText = view.findViewById(R.id.dialog_text);

        // Fetch arguments from bundle and set title
        String label = getArguments().getString("label", "(No answer found)");
      //  getDialog().setTitle("DONDE SE VE ESTO?");
        dialogLabel.setText(label);

        String text = getArguments().getString("text","(No text found)");
        dialogText.setText(text);

        txtSite = view.findViewById(R.id.txtSite);
        txtSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.pregnancy_test_website))));
            }
        });
    }

}
